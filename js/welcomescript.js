var backbtn=document.getElementById('backbtn');
backbtn.addEventListener("click",function(){
	window.open('form.html','_self');
})

const urlParams = new URLSearchParams(window.location.search);

var fullname=document.getElementById('fullname');
const myfirst = urlParams.get('first');
const mylast = urlParams.get('last');
fullname.innerHTML = `${myfirst} ${mylast}`;

var phone=document.getElementById('phone');
const myphone = urlParams.get('phone');
phone.innerHTML = myphone;

//Gender
var gender=document.getElementById('gender');
const mygender = urlParams.get('gender');
gender.innerHTML = mygender;

var dob=document.getElementById('dob');
const mydob = urlParams.get('dob');
dob.innerHTML = mydob;

var job=document.getElementById('job');
const myjob = urlParams.get('job');
job.innerHTML = myjob;

var company=document.getElementById('company');
const mycompany = urlParams.get('company');
company.innerHTML = mycompany;

var scanbtn=document.getElementById('scanbtn');
scanbtn.addEventListener("click",function(){
	window.open('scan.html','_self');
})